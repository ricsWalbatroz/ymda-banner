<?php

/*
Plugin Name: YMDA Banner
Plugin URI: http://ymda.com/
Description: Este plugin adiciona banners as sidebar.
Version: 1.0
Author: Guilherme Souza
Author URI: http://grsouza.me
License: A "Slug" license name e.g. GPL2
*/

function ymda_register_banner_widget() {
	require_once( 'lib/Ymda_Banner_Widget.php' );
	register_widget( 'Ymda_Banner_Widget' );
}
add_action( 'widgets_init', 'ymda_register_banner_widget' );




// Register Custom Post Type
function ymda_banner_post_type() {

	$labels = array(
		'name'                => 'Banners',
		'singular_name'       => 'Banner',
		'menu_name'           => 'Banners',
		'name_admin_bar'      => 'Banners',
	);
	$args = array(
		'label'               => 'ymda_banner_post_type',
		'description'         => 'Banners',
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail' ),
		'taxonomies'          => array( 'banner_category' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'			  => 'dashicons-grid-view',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'banner', $args );

}

// Hook into the 'init' action
add_action( 'init', 'ymda_banner_post_type', 0 );



// Register Custom Taxonomy
function ymda_banner_taxonomy() {

	$labels = array(
		'name'                       => 'Categorias do Banner',
		'singular_name'              => 'Categoria do Banner',
		'menu_name'                  => 'Categorias',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'banner_category', array( 'banner' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'ymda_banner_taxonomy', 0 );


/**
 * Adicionar Meta Box
 */
function ymda_banner_add_link_metabox() {
	add_meta_box( 'ymda_banner_link_metabox', 'Link do Banner', 'ymda_banner_link_metabox', 'banner', 'advanced', 'high' );
}

add_action( 'add_meta_boxes', 'ymda_banner_add_link_metabox' );

/**
 * Callback function para Link Metabox
 */
function ymda_banner_link_metabox() {
	global $post;
	$custom = get_post_custom( $post->ID );
	$ymda_banner_link = isset( $custom['ymda_banner_link'][0] ) ? $custom['ymda_banner_link'][0]:'';

	echo '<label for="ymda_banner_link">Link do banner</label> ';
	echo '<input type="text" name="ymda_banner_link" value="'. esc_url( $ymda_banner_link ) .'">';
}

/**
 * Fun��o para salvar o link da metabox
 */

function ymda_banner_save_link() {
	if( empty($_POST) ) return;
	global $post;
	update_post_meta($post->ID, 'ymda_banner_link', $_POST['ymda_banner_link']);
}

add_action( 'save_post', 'ymda_banner_save_link' );